var sequence = null;
var rows=0;
var cols=0;
var bpm=120;
var grid=[];
var cursor=0;
var player=[];
var players=16;
var lastPlayer = 0;
var posRow=0;
var posCol=0;

$(document).ready(function(){
    init();
    initPad();
    $("#start").click(startSequence);
    $("#stop").click(stopSequence);
    $("#rows").change(initPad);
    $("#cols").change(initPad);
    $("#bpm").change(setSequence);
});

function startSequence() {
    cursor = 0;
    setSequence();
    $("#power div").addClass("led-green");
}
function stopSequence() {
    clearInterval(sequence);
    $("#power div").removeClass("led-green");
    sequence = null;
}
function setSequence() {
    bpm = $("#bpm").val();
    if (sequence != null) {
        clearInterval(sequence);
    }
    timing = 15000 / bpm;
    sequence = setInterval(iterator,timing);
}

function updDisplay() {
    thisRow = Math.floor(cursor/cols);
    thisCol = cursor%cols;

    var beat = "";
    if (grid[thisRow][thisCol] != 0) {
        beat += "*Kick*";
    }
    var output = "Row: "+(thisRow+1)+" Col: "+(thisCol+1)+" "+beat;
    output += "<br/>";
    output += "BPM: "+bpm;

    $("#display").html(output);
}

function iterator () {
    thisRow = Math.floor(cursor/cols);
    thisCol = cursor%cols;

    $(".cell").removeClass("current");
    $("#r"+thisRow+"c"+thisCol).addClass("current");

    if (readGrid(thisRow,thisCol,1)) {
        $("#indicator").addClass("led-red");
        setTimeout(function(){
            $("#indicator").removeClass("led-red");
        },timing *.5);
        playSound();
    }

    updDisplay();
    // Increment cursor and reset if outside grid
    if (++cursor >= (rows*cols)) {
        cursor = 0;
    }
}

function readGrid(thisRow,thisCol,bit) {
    var gridBits = grid[thisRow][thisCol].toString(2);
    var index = gridBits.length-bit;
    if (gridBits[index] == "1") {
        return true;
    } else {
        return false;
    }
}


function padClick() {
    match = this.id.match(/r([0-9])+c([0-9])+/);
    togglePad(match[1],match[2]);
}
function togglePad(thisRow,thisCol) {
    if (grid[thisRow][thisCol] == 0) {
        grid[thisRow][thisCol] = 1
        $("#r"+thisRow+"c"+thisCol).addClass("active");
    } else {
        grid[thisRow][thisCol] = 0
        $("#r"+thisRow+"c"+thisCol).removeClass("active");
    }
}


function playSound() {
    player[lastPlayer++].play();
    if (lastPlayer > 7) lastPlayer=0;
}

function init() {
    for (row=0;row < rows;row++) {
        grid[row]=[];
        for (col=0;col < cols;col++) {
            grid[row][col]=0;
        }
    }
    for (nr=0;nr<players;nr++) {
        player[nr] = new Audio("sound/Kick05.wav");
    }
}

function initPad () {
    rows = $("#rows").val();
    cols = $("#cols").val();
    drawPad();
}
function drawPad () {
    var pad = $("#pad");
    pad.removeClass();
    pad.addClass("rows"+rows);
    pad.addClass("cols"+cols);
    pad.html("");
    for (thisRow=0;thisRow < rows;thisRow++) {
        if (typeof(grid[thisRow]) == "undefined") grid[thisRow]=[];
        var rowData = '';
        for (thisCol=0;thisCol < cols;thisCol++) {
            if (typeof(grid[thisRow][thisCol]) == "undefined") grid[thisRow][thisCol]=0;
            var act = "";
            if (grid[thisRow][thisCol] == 1) act = ' active';
            rowData += '<div id="r'+thisRow+'c'+thisCol+'" class="cell r'+thisRow+' c'+thisCol+' '+act+'"> </div>';
        }
        pad.append(rowData);
    }
    $(".cell").click(padClick);
}
function updMarker() {
    $("#marker").removeClass();
    $("#marker").addClass("r"+posRow);
    $("#marker").addClass("c"+posCol);
}


$(document).keypress(function(event){
    switch (event.key) {
        case " ":
            togglePad(posRow,posCol);
            event.preventDefault();
            break;
        case "q":
            togglePad(posRow,0);
            break;
        case "w":
            togglePad(posRow,1);
            break;
        case "e":
            togglePad(posRow,2);
            break;
        case "r":
            togglePad(posRow,3);
            break;
        case "t":
            togglePad(posRow,4);
            break;
        case "y":
            togglePad(posRow,5);
            break;
        case "u":
            togglePad(posRow,6);
            break;
        case "i":
            togglePad(posRow,7);
            break;
        case "o":
            togglePad(posRow,8);
            break;
        case "p":
            togglePad(posRow,9);
            break;
        case "å":
            togglePad(posRow,10);
            break;
        case "Left": // Left
            if (--posCol < 0) posCol = 0;
            event.preventDefault();
            break;
        case "Up": // Up
            if (--posRow < 0) posRow = 0;
            event.preventDefault();
            break;
        case "Right": // Right
            if (++posCol >= cols) posCol = cols-1;
            event.preventDefault();
            break;
        case "Down": // Down
            if (++posRow >= rows) posRow = rows-1;
            event.preventDefault();
            break;
    }
    updMarker();
});

